package main

import (
	"github.com/schollz/progressbar/v3"
	"github.com/xanzy/go-gitlab"
)

func listUsers(config GitlabConfig) ([]*gitlab.User, error) {
	git, err := gitlab.NewClient(config.Token, gitlab.WithBaseURL(config.Endpoint))
	if err != nil {
		return nil, err
	}

	opt := &gitlab.ListUsersOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 10,
			Page:    1,
		},
		// Active:             gitlab.Ptr(true),
		// Blocked:            gitlab.Ptr(false),
		// WithoutProjectBots: gitlab.Ptr(true),
		Sort:    gitlab.Ptr("asc"),
		OrderBy: gitlab.Ptr("username"),
	}

	allUsers := []*gitlab.User{}

	bar := progressbar.NewOptions(
		100,
		progressbar.OptionClearOnFinish(),
		progressbar.OptionShowCount(),
		progressbar.OptionEnableColorCodes(true),
		progressbar.OptionSetDescription("[cyan]Fetching Users list[reset]..."),
	)

	for {
		// Get the first page with projects.
		users, resp, err := git.Users.ListUsers(opt)
		if err != nil {
			return nil, err
		}

		if resp.PreviousPage == 0 {
			bar.ChangeMax64(int64(resp.TotalItems))
		}
		_ = bar.Add(len(users))

		allUsers = append(allUsers, users...)

		// Exit the loop when we've seen all pages.
		if resp.NextPage == 0 {
			break
		}

		// Update the page number to get the next page.
		opt.Page = resp.NextPage
	}

	return allUsers, nil
}

func getUserIDMap(config GitlabConfig) (map[int]*gitlab.User, error) {
	usersByID := map[int]*gitlab.User{}

	users, err := listUsers(config)
	if err != nil {
		return nil, err
	}
	for _, user := range users {
		usersByID[user.ID] = user
	}

	return usersByID, nil
}
