# gitlab-token-dump

Displays tokens from a GitLab instance.

## Usage

```
Usage of ./bin/gtd_linux_amd64:
  -a    Show inactive tokens (default: false) (default true)
  -e    Show expired tokens (default: false)
  -expirein duration
        List tokens expiring in X, using golang duration specifier (default: none)
  -g string
        Gitlab endpoint (default GITLAB_ENDPOINT env var "https://gitlab.com" if not set)
  -l    Gitlab token type list
  -r    Show revoked tokens (default: false)
  -t string
        Gitlab token type to dump (default: personal) (default "personal")
  -token string
        Gitlab token (default: GITLAB_TOKEN env var) (default "glpat-VV-PtMFsGDWKK8PoWtR8")
  -u string
        Gitlab user (default: all users)
```

Supported token types:

- personal
- project
- group
- deploy-project
- all (all of the above)

## Example

```
# list personal access tokens for user user1
$ gtd -t personal -u user1
  ID  |      NAME      | OWNER |            SCOPES            | REVOKED | ACTIVE |      LASTUSED       |  EXPIRES
------+----------------+-------+------------------------------+---------+--------+---------------------+-------------
  123 | registry token | user1 | read_registry,write_registry | false   | true   | 2024-06-13 21:10:05 | 2025-06-13
  124 | api_token_ro   | user1 | read_api                     | false   | true   | 2024-06-14 17:17:49 | 2025-06-13

# list all personal access tokens for all users (admin access required or you will only see your own tokens)
$ gtd -t personal
  ID  |      NAME      | OWNER |            SCOPES            | REVOKED | ACTIVE |      LASTUSED       |  EXPIRES
------+----------------+-------+------------------------------+---------+--------+---------------------+-------------
  123 | registry token | user1 | read_registry,write_registry | false   | true   | 2024-06-13 21:10:05 | 2025-06-19
  124 | api_token_ro   | user1 | read_api                     | false   | true   | 2024-06-14 17:17:49 | 2025-07-13
  125 | api_token_rw   | user2 | read_api,write_api           | false   | true   | 2024-06-14 17:17:49 | 2025-07-13

# list only personal access tokens that expire in less than 10 days
$ gtd -t personal -expirein 240h
  ID  |      NAME      | OWNER |            SCOPES            | REVOKED | ACTIVE |      LASTUSED       |  EXPIRES
------+----------------+-------+------------------------------+---------+--------+---------------------+-------------
  123 | registry token | user1 | read_registry,write_registry | false   | true   | 2024-06-13 21:10:05 | 2025-06-19
```
