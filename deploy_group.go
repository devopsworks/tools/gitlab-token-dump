package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/schollz/progressbar/v3"
	"github.com/xanzy/go-gitlab"
)

type GroupDeployToken struct {
	Group *gitlab.Group
	Token *gitlab.DeployToken
}

func listDeployGroupTokens(config GitlabConfig) error {
	tokens, err := getDeployGroupTokens(config)
	if err != nil {
		log.Fatalf("Failed to list project tokens: %v", err)
	}

	lines := [][]string{}

	for _, token := range tokens {
		expiresAt := ""
		if token.Token.ExpiresAt != nil {
			expiresAt = time.Time(*token.Token.ExpiresAt).Format("2006-01-02")
		}

		// Skip expired tokens if requested
		if !config.ShowExpired && token.Token.ExpiresAt != nil && time.Time(*token.Token.ExpiresAt).Before(time.Now()) {
			continue
		}

		// Skip revoked tokens if requested
		if !config.ShowRevoked && token.Token.Revoked {
			continue
		}

		// Skip not expiring tokens if requested
		if config.ExpiresIn != nil && token.Token.ExpiresAt != nil && time.Time(*token.Token.ExpiresAt).After(time.Now().Add(*config.ExpiresIn)) {
			continue
		}
		lines = append(lines, []string{
			fmt.Sprintf("%d", token.Token.ID),
			token.Token.Name,
			token.Token.Username,
			fmt.Sprintf("%s (%s)", token.Group.Name, token.Group.FullPath),
			strings.Join(token.Token.Scopes, ","),
			fmt.Sprintf("%t", token.Token.Revoked),
			expiresAt,
		})
	}

	printTable([]string{"ID", "Name", "Username", "Group", "Scopes", "Revoked", "Expires"}, lines)

	return nil
}

func getDeployGroupTokens(config GitlabConfig) ([]GroupDeployToken, error) {
	git, err := gitlab.NewClient(config.Token, gitlab.WithBaseURL(config.Endpoint))
	if err != nil {
		return nil, err
	}

	allDeployTokens := []GroupDeployToken{}

	grp, err := listGroups(config)
	if err != nil {
		return nil, err
	}

	opt := &gitlab.ListGroupDeployTokensOptions{}

	bar := progressbar.NewOptions(
		len(grp),
		progressbar.OptionClearOnFinish(),
		progressbar.OptionShowCount(),
		progressbar.OptionEnableColorCodes(true),
		progressbar.OptionSetDescription("[cyan]Fetching group deploy tokens[reset]..."),
	)
	for _, g := range grp {

		_ = bar.Add(1)

		for {
			// Get the first page with projects.
			tokens, resp, err := git.DeployTokens.ListGroupDeployTokens(g.ID, opt)
			if err != nil {
				return nil, err
			}

			for _, token := range tokens {
				allDeployTokens = append(allDeployTokens, GroupDeployToken{Group: g, Token: token})
			}

			// Exit the loop when we've seen all pages.
			if resp.NextPage == 0 {
				break
			}

			// Update the page number to get the next page.
			opt.Page = resp.NextPage
		}
	}

	return allDeployTokens, nil
}
