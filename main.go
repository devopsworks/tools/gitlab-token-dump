package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"time"
)

type GitlabConfig struct {
	Endpoint     string
	Token        string
	ShowExpired  bool
	ShowRevoked  bool
	ShowInactive bool
	ExpiresIn    *time.Duration
}

var Version string

func main() {

	var gitlabTokenTypes = []string{"personal", "project", "group", "deploy-project", "all"}

	defaultEndpoint := os.Getenv("GITLAB_ENDPOINT")
	if defaultEndpoint == "" {
		defaultEndpoint = "https://gitlab.com"
	}

	// Gitlab endpoint
	var gitlabEndpoint = flag.String("g", defaultEndpoint, "Gitlab endpoint")
	// Gitlab token from cli or env
	var gitlabToken = flag.String("token", os.Getenv("GITLAB_TOKEN"), "Gitlab token (default: GITLAB_TOKEN env var)")
	// Gitlab token type to dump
	var gitlabTokenType = flag.String("t", "personal", "Gitlab token type to dump (default: personal)")
	// Gitlab token type list
	var gitlabTokenTypeList = flag.Bool("l", false, "Gitlab token type list")
	// User
	var gitlabUser = flag.String("u", "", "Gitlab user (default: all users)")
	// Show expired
	var gitlabShowExpired = flag.Bool("e", false, "Show expired tokens (default: false)")
	// Show revoked
	var gitlabShowRevoked = flag.Bool("r", false, "Show revoked tokens (default: false)")
	// Show active
	var gitlabShowInactive = flag.Bool("a", true, "Show inactive tokens (default: false)")
	// List tokens expiring in N days
	var gitlabExpiresIn = flag.Duration("expirein", 0, "List tokens expiring in X, using golang duration specifier (default: disabled)")
	// Show version
	var version = flag.Bool("v", false, "Show version")

	flag.Parse()

	if *version {
		fmt.Println(Version)
		os.Exit(0)
	}

	if *gitlabTokenTypeList {
		fmt.Println("Gitlab token types:")
		for _, tokenType := range gitlabTokenTypes {
			fmt.Printf("  - %s\n", tokenType)
		}
	}

	// Check token
	if *gitlabToken == "" {
		log.Fatalf("Gitlab token is required")
	}

	config := GitlabConfig{
		Endpoint:     *gitlabEndpoint,
		Token:        *gitlabToken,
		ShowExpired:  *gitlabShowExpired,
		ShowRevoked:  *gitlabShowRevoked,
		ShowInactive: *gitlabShowInactive,
	}

	if *gitlabExpiresIn != 0 {
		config.ExpiresIn = gitlabExpiresIn
	}

	switch *gitlabTokenType {
	case "personal":
		err := listPersonalAccessTokens(config, *gitlabUser)
		if err != nil {
			log.Fatalf("Failed to list personal access tokens: %v", err)
		}
	case "project":
		err := listProjectAccessTokens(config)
		if err != nil {
			log.Fatalf("Failed to list project access tokens: %v", err)
		}
	case "group":
		err := listGroupAccessTokens(config)
		if err != nil {
			log.Fatalf("Failed to list group access tokens: %v", err)
		}
	case "deploy-project":
		err := listDeployProjectTokens(config)
		if err != nil {
			log.Fatalf("Failed to list project deploy tokens: %v", err)
		}
	case "deploy-group":
		err := listDeployGroupTokens(config)
		if err != nil {
			log.Fatalf("Failed to list group deploy tokens: %v", err)
		}
	case "all":
		err := listPersonalAccessTokens(config, *gitlabUser)
		if err != nil {
			log.Fatalf("Failed to list personal access tokens: %v", err)
		}
		err = listProjectAccessTokens(config)
		if err != nil {
			log.Fatalf("Failed to list project access tokens: %v", err)
		}
		err = listGroupAccessTokens(config)
		if err != nil {
			log.Fatalf("Failed to list group access tokens: %v", err)
		}
		err = listDeployProjectTokens(config)
		if err != nil {
			log.Fatalf("Failed to list project deploy tokens: %v", err)
		}
		err = listDeployGroupTokens(config)
		if err != nil {
			log.Fatalf("Failed to list group deploy tokens: %v", err)
		}
	}
}
