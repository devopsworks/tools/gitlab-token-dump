package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/schollz/progressbar/v3"
	"github.com/xanzy/go-gitlab"
)

func listPersonalAccessTokens(config GitlabConfig, user string) error {
	// git, err := gitlab.NewClient(config.Token, gitlab.WithBaseURL(config.Endpoint))
	// if err != nil {
	// 	return err
	// }

	usersByID := map[int]*gitlab.User{}

	u := &gitlab.User{}

	users, err := listUsers(config)
	if err != nil {
		log.Fatalf("Failed to list users: %v", err)
	}
	for _, user := range users {
		usersByID[user.ID] = user
	}
	// 	u, _, err = git.Users.CurrentUser()
	// 	if err != nil {
	// 		log.Fatalf("Failed to get current user: %v", err)
	// 	}
	// } else {
	if user != "" {
		for _, gu := range users {
			if user == gu.Username {
				u = gu
				break
			}
		}

		if u.Username != user {
			log.Fatalf("User not found: %s", user)
		}
	}

	tokens, err := getUserTokens(config, u)
	if err != nil {
		log.Fatalf("Failed to list user tokens: %v", err)
	}

	lines := [][]string{}

	for _, token := range tokens {
		owner := fmt.Sprintf("unknown [%d]", token.UserID)
		if user, ok := usersByID[token.UserID]; ok {
			owner = user.Username
		}

		lastUsed := ""
		if token.LastUsedAt != nil {
			lastUsed = time.Time(*token.LastUsedAt).Format("2006-01-02 15:04:05")
		}
		expiresAt := ""
		if token.ExpiresAt != nil {
			expiresAt = time.Time(*token.ExpiresAt).Format("2006-01-02")
		}

		// Skip expired tokens if requested
		if !config.ShowExpired && token.ExpiresAt != nil && time.Time(*token.ExpiresAt).Before(time.Now()) {
			continue
		}

		// Skip revoked tokens if requested
		if !config.ShowRevoked && token.Revoked {
			continue
		}

		// Skip inactive tokens if requested
		if !config.ShowInactive && !token.Active {
			continue
		}

		// Skip not expiring tokens if requested
		if config.ExpiresIn != nil && token.ExpiresAt != nil && time.Time(*token.ExpiresAt).After(time.Now().Add(*config.ExpiresIn)) {
			continue
		}

		lines = append(lines, []string{
			fmt.Sprintf("%d", token.ID),
			token.Name,
			owner,
			strings.Join(token.Scopes, ","),
			fmt.Sprintf("%t", token.Revoked),
			fmt.Sprintf("%t", token.Active),
			lastUsed,
			expiresAt,
		})

		// fmt.Printf("ID: %d, Name: %q, User: %s, Scopes: %s, Revoked: %t, Active: %t, LastUsed: %s, Expires: %s\n", token.ID, token.Name, owner, token.Scopes, token.Revoked, token.Active, token.LastUsedAt, token.ExpiresAt)
	}
	printTable([]string{"ID", "Name", "Owner", "Scopes", "Revoked", "Active", "LastUsed", "Expires"}, lines)
	// fmt.Printf("%+v\n", tokens)

	return nil
}

func getUserTokens(config GitlabConfig, user *gitlab.User) ([]*gitlab.PersonalAccessToken, error) {
	git, err := gitlab.NewClient(config.Token, gitlab.WithBaseURL(config.Endpoint))
	if err != nil {
		return nil, err
	}

	opt := &gitlab.ListPersonalAccessTokensOptions{
		UserID: gitlab.Ptr(user.ID),
	}

	allUserTokens := []*gitlab.PersonalAccessToken{}

	bar := progressbar.NewOptions(
		100,
		progressbar.OptionClearOnFinish(),
		progressbar.OptionEnableColorCodes(true),
		progressbar.OptionShowCount(),
		progressbar.OptionSetDescription("[cyan]Fetching personal access tokens[reset]..."),
	)

	for {
		// Get the first page with projects.
		tokens, resp, err := git.PersonalAccessTokens.ListPersonalAccessTokens(opt)
		if err != nil {
			return nil, err
		}

		if resp.PreviousPage == 0 {
			bar.ChangeMax64(int64(resp.TotalItems))
		}
		_ = bar.Add(len(tokens))

		allUserTokens = append(allUserTokens, tokens...)

		// Exit the loop when we've seen all pages.
		if resp.NextPage == 0 {
			break
		}

		// Update the page number to get the next page.
		opt.Page = resp.NextPage
	}

	return allUserTokens, nil
}
