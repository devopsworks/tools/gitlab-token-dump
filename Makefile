PACKAGE=gtd
WINDOWS=bin/$(PACKAGE)_windows_amd64.exe
LINUX=bin/$(PACKAGE)_linux_amd64
DARWIN=bin/$(PACKAGE)_darwin_amd64
VERSION=$(shell git describe --tags --always --long --dirty)

.PHONY: all test clean

all: test build ## Build and run tests

test: ## Run unit tests
	go test ./...

build: windows linux darwin ## Build binaries
	@echo version: $(VERSION)

windows: $(WINDOWS)-$(VERSION) ## Build for Windows

linux: $(LINUX)-$(VERSION) ## Build for Linux

darwin: $(DARWIN)-$(VERSION) ## Build for Darwin (macOS)

$(WINDOWS)-$(VERSION):
	env GOOS=windows GOARCH=amd64 go build -o $(WINDOWS)-$(VERSION) -ldflags="-s -w -X main.Version=$(VERSION)" .

$(LINUX)-$(VERSION):
	env GOOS=linux GOARCH=amd64 go build -o $(LINUX)-$(VERSION) -ldflags="-s -w -X main.Version=$(VERSION)" .

$(DARWIN)-$(VERSION):
	env GOOS=darwin GOARCH=amd64 go build -o $(DARWIN)-$(VERSION) -ldflags="-s -w -X main.Version=$(VERSION)" .

clean: ## Remove previous build
	rm -f bin/*

help: ## Display available commands
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

release: clean windows darwin linux ## Build program binaries
	strip $(LINUX)-$(VERSION)
	(cd bin && sha256sum * > SHA256SUMS)
	cp $(LINUX)-$(VERSION) bin/$(PACKAGE)
	gzip $(LINUX)-$(VERSION)
	gzip $(DARWIN)-$(VERSION)
	gzip $(WINDOWS)-$(VERSION)
	./bin/$(PACKAGE) -v