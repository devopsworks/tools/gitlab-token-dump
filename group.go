package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/schollz/progressbar/v3"
	"github.com/xanzy/go-gitlab"
)

func listGroupAccessTokens(config GitlabConfig) error {
	usersByID, err := getUserIDMap(config)
	if err != nil {
		return err
	}

	tokens, err := getGroupTokens(config)
	if err != nil {
		log.Fatalf("Failed to list group tokens: %v", err)
	}

	lines := [][]string{}

	for _, token := range tokens {
		owner := fmt.Sprintf("unknown [%d]", token.UserID)
		if user, ok := usersByID[token.UserID]; ok {
			owner = user.Username
		}

		lastUsed := ""
		if token.LastUsedAt != nil {
			lastUsed = time.Time(*token.LastUsedAt).Format("2006-01-02 15:04:05")
		}
		expiresAt := ""
		if token.ExpiresAt != nil {
			expiresAt = time.Time(*token.ExpiresAt).Format("2006-01-02")
		}

		// Skip expired tokens if requested
		if !config.ShowExpired && token.ExpiresAt != nil && time.Time(*token.ExpiresAt).Before(time.Now()) {
			continue
		}

		// Skip revoked tokens if requested
		if !config.ShowRevoked && token.Revoked {
			continue
		}

		// Skip inactive tokens if requested
		if !config.ShowInactive && !token.Active {
			continue
		}

		// Skip not expiring tokens if requested
		if config.ExpiresIn != nil && token.ExpiresAt != nil && time.Time(*token.ExpiresAt).After(time.Now().Add(*config.ExpiresIn)) {
			continue
		}

		lines = append(lines, []string{
			fmt.Sprintf("%d", token.ID),
			token.Name,
			owner,
			strings.Join(token.Scopes, ","),
			fmt.Sprintf("%d", token.AccessLevel),
			fmt.Sprintf("%t", token.Revoked),
			fmt.Sprintf("%t", token.Active),
			lastUsed,
			expiresAt,
		})

		// fmt.Printf("ID: %d, Name: %q, User: %s, Scopes: %s, Revoked: %t, Active: %t, LastUsed: %s, Expires: %s\n", token.ID, token.Name, owner, token.Scopes, token.Revoked, token.Active, token.LastUsedAt, token.ExpiresAt)
	}
	printTable([]string{"ID", "Name", "Owner", "Scopes", "Access Lvl", "Revoked", "Active", "LastUsed", "Expires"}, lines)
	// fmt.Printf("%+v\n", tokens)

	return nil
}

func getGroupTokens(config GitlabConfig) ([]*gitlab.GroupAccessToken, error) {
	git, err := gitlab.NewClient(config.Token, gitlab.WithBaseURL(config.Endpoint))
	if err != nil {
		return nil, err
	}

	opt := &gitlab.ListGroupAccessTokensOptions{}
	allGroupTokens := []*gitlab.GroupAccessToken{}

	projects, err := listGroups(config)
	if err != nil {
		return nil, err
	}

	bar := progressbar.NewOptions(
		len(projects),
		progressbar.OptionClearOnFinish(),
		progressbar.OptionShowCount(),
		progressbar.OptionEnableColorCodes(true),
		progressbar.OptionSetDescription("[cyan]Fetching projects tokens[reset]..."),
	)
	for _, prj := range projects {

		// fmt.Println("listing project tokens for project:", prj.ID)
		_ = bar.Add(1)
		for {
			// Get the first page with projects.
			tokens, resp, err := git.GroupAccessTokens.ListGroupAccessTokens(prj.ID, opt)
			if err != nil {
				return nil, err
			}

			allGroupTokens = append(allGroupTokens, tokens...)

			// Exit the loop when we've seen all pages.
			if resp.NextPage == 0 {
				break
			}

			// Update the page number to get the next page.
			opt.Page = resp.NextPage
		}
	}

	return allGroupTokens, nil
}

func listGroups(config GitlabConfig) ([]*gitlab.Group, error) {
	git, err := gitlab.NewClient(config.Token, gitlab.WithBaseURL(config.Endpoint))
	if err != nil {
		return nil, err
	}

	opt := &gitlab.ListGroupsOptions{}
	allGroups := []*gitlab.Group{}

	bar := progressbar.NewOptions(
		100,
		progressbar.OptionClearOnFinish(),
		progressbar.OptionShowCount(),
		progressbar.OptionEnableColorCodes(true),
		progressbar.OptionSetDescription("[cyan]Fetching group list[reset]..."),
	)
	for {
		// Get the first page with projects.
		grps, resp, err := git.Groups.ListGroups(opt)
		if err != nil {
			return nil, err
		}
		if resp.PreviousPage == 0 {
			bar.ChangeMax64(int64(resp.TotalItems))
		}

		_ = bar.Add(len(grps))

		allGroups = append(allGroups, grps...)

		// Exit the loop when we've seen all pages.
		if resp.NextPage == 0 {
			break
		}

		// Update the page number to get the next page.
		opt.Page = resp.NextPage
	}

	return allGroups, nil
}
